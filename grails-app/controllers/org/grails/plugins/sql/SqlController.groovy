package org.grails.plugins.sql

import groovy.sql.Sql
import javax.sql.DataSource

class SqlController {
    DataSource dataSource;

    def index() {
        render view: 'index', model: [sql: '']
    }

    def exec(String query) {
        def list
        def sql
        try {
            sql = Sql.newInstance(dataSource)
            list = sql.rows(query.toString());
        } finally {
            sql?.close()
        }
        render view: 'index', model: [query: query, result: list]
    }
}
