<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>

<body>
<g:form action="exec">
<g:field name="query" size="120" value="${sql}"/>
    <g:submitButton name="Submit"/>
</g:form>
<g:if test="${result}">
    <table>
        <g:each in="${result}" var="rec">
            <tr>
                <g:each in="${rec.values()}" var="col">
                    <td>${col}</td>
                </g:each>
            </tr>
        </g:each>
    </table>
</g:if>
</body>
</html>
